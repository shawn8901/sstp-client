{
  description = "";

  # Nixpkgs / NixOS version to use.
  inputs.nixpkgs.url = "nixpkgs/nixos-23.11";

  outputs =
    { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in
    {
      devShells.${system}.default = pkgs.mkShell {
        # Update the name to something that suites your project.
        name = "sstp-client";

        packages = with pkgs; [
          automake
          autogen
          autoconf
          pkg-config
          libtool
          libevent.dev
          openssl.dev
          ppp
        ];
      };
    };
}
